﻿using FindStudent.Helpers;
using FindStudent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.MongoServices
{
    public class UserService
    {
        private readonly MongoDBHelper<Models.User> _users;

        public UserService()
        {
            _users = new MongoDBHelper<Models.User>();
        }

        public void Create(User details)
        {
            _users.Collection.InsertOne(details);
        }
    }
}