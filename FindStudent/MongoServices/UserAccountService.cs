﻿using FindStudent.Helpers;
using FindStudent.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.MongoServices
{
    public class UserAccountService
    {
        private readonly MongoDBHelper<Models.UserAccount> _usersAccounts;

        public UserAccountService()
        {
            _usersAccounts = new MongoDBHelper<Models.UserAccount>();
        }

        public void Create(UserAccount account)
        {
            _usersAccounts.Collection.InsertOne(account);
        }
    }
}