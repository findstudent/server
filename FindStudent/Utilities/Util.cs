﻿using FindStudent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace FindStudent.Utilities
{
    public static class Util
    {
        public static String Language;
        public static String GoogleMapsApiURL = "https://maps.googleapis.com/maps/api/geocode/json?";


        public static LocationDetails GetGoogleLocationDetails(Address coordinate, String language)
        {
            String coordinateURL = MountCoordinateURL(coordinate);
            String typeURL = MountTypesURL();
            String languageURL = MountLanguageURL(language);
            String keyURL = MountKeyURL();
            String locationURL = GoogleMapsApiURL + coordinateURL + typeURL + languageURL + keyURL;

            var request = (HttpWebRequest)WebRequest.Create(locationURL);
            var response = (HttpWebResponse)request.GetResponse();

            return null;
        }

        public static String MountCoordinateURL(Address coordinate)
        {
            return "latlng=" + coordinate.Latitude + "," + coordinate.Longitude;
        }

        public static String MountTypesURL()
        {
            return "&result_type=country|administrative_area_level_1";
        }

        public static String MountLanguageURL(String language)
        {
            return "&language=" + language;
        }

        public static String MountKeyURL()
        {
            return "key=" + ;
        }
    }
}