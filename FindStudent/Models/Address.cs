﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class Address
    {
        public Double Longitude { get; set; }

        public Double Latitude { get; set; }
    }
}