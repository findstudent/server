﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public static class LanguagesSuported
    {
        public const String English = "en";
        public const String Portuguese = "pt";
    }
}