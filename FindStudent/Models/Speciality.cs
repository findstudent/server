﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
	public class Speciality
	{
        public ObjectId SpecialityIdentifier { get; set; }

        public String Language { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public Area Under { get; set; }
    }
}