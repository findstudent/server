﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class LocationDetails
    {
        //public ObjectId LocationIdentifier { get; set; }

        public String Language { get; set; }

        public String Country { get; set; }

        public String State { get; set; }

        public String City { get; set; }
    }
}