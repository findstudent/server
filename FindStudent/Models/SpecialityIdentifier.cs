﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class SpecialityIdentifier
    {
        [BsonId]
        public ObjectId Id { get; set; }
    }
}