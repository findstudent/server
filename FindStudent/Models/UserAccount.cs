﻿using FindStudent.MongoServices;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class UserAccount
    {
        [BsonId]
        public String Email { get; set; }

        public String FacebookId { get; set; }

        public void Create()
        {
            UserAccountService accountService = new UserAccountService();
            accountService.Create(this);
        }
    }

    public class UserToRegister : UserAccount
    {
        public User Details { get; set; }
    }
}