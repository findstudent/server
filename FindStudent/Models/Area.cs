﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class Area
    {
        public ObjectId AreaIdentifier { get; set; }

        public String Language { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }
    }
}