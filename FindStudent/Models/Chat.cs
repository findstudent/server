﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class Chat
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public String SenderEmail { get; set; }

        public String ReceiverEmail { get; set; }

        public String Message { get; set; }
    }
}