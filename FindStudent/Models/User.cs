﻿using FindStudent.MongoServices;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Models
{
    public class User
    {
        [BsonId]
        public String Email { get; set; }

        public String Name { get; set; }

        public String LastName { get; set; }

        public int Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Address Home { get; set; }

        public List<LocationDetails> DetailsInLanguages { get; set; }

        public List<SpecialityIdentifier> KnownSpecialities { get; set; }

        public List<SpecialityIdentifier> LearnSpecialities { get; set; }

        public void Create()
        {
            UserService userService = new UserService();

            //Get All the countries, states and cities by all the languages supported by the system.

            userService.Create(this);
        }

        public List<LocationDetails> GetGoogleLocationDetails()
        {


            return null;
        }

    }

    public static class GenderTerminology
    {
        public const int Masculine = 1;
        public const int Feminine = 2;
    }
}