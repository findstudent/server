﻿using FindStudent.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FindStudent
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            #region DEFINE CULTURE
            /*String culture = HttpContext.Current.Items["Culture"].ToString();
            //String culture = HttpContext.Current.Request.ServerVariables.Get("HTTP_ACCEPT_LANGUAGE");

            if (culture == null)
                culture = "en";

            if (culture.Contains("pt"))
                Utilities.Util.Language = LanguagesSuported.pt_br;
            else
                Utilities.Util.Language = LanguagesSuported.eng;

            /*CultureInfo information = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = information;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(information.Name);*/
            #endregion DEFINE CULTURE
        }

        private void Application_BeginRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            String culture = null;
            if (context.Request.UserLanguages != null && Request.UserLanguages.Length > 0)
                culture = Request.UserLanguages[0];

            switch (culture)
            {
                case LanguagesSuported.Portuguese:
                    Utilities.Util.Language = LanguagesSuported.Portuguese;
                    break;                   
                default:
                    culture = LanguagesSuported.English;
                    Utilities.Util.Language = LanguagesSuported.English;
                    break;
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}
