﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FindStudent.Helpers
{
    public class MongoDBHelper<T> where T : class
    {
        public IMongoCollection<T> Collection { get; private set; }

        public MongoDBHelper()
        {
            MongoClient mongoClient = new MongoClient("mongodb://mongodbserverjarvis.cloudapp.net");
            IMongoDatabase findStudentDB = mongoClient.GetDatabase("findStudent");
            this.Collection = findStudentDB.GetCollection<T>(typeof(T).Name);
        }
    }
}