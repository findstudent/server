﻿using FindStudent.Models;
using FindStudent.MongoServices;
using FindStudent.Resources;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FindStudent.Controllers
{
    public class UserController : BaseController
    {
        public HttpResponseMessage Post(UserToRegister user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UserAccount account = user;
                    account.Create();

                    User details = user.Details;
                    details.Create();

                    return Request.CreateResponse(HttpStatusCode.Created, Resource.UserAddedSuccess);
                }
                catch (MongoWriteException ex)
                {
                    switch (ex.WriteError.Category)
                    {
                        case ServerErrorCategory.DuplicateKey:
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Resource.ErrorDuplicateKeyUser);
                        case ServerErrorCategory.ExecutionTimeout:
                            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Resource.ErrorTimeOut);
                        default:
                            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, Resource.SystemError);
                    }
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
